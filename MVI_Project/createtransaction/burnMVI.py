from web3 import Web3
from interface import getInterface
from MVIIndexAbi import getMVIIndexAbi
from MVIAbi import getMVIIAbi
import threading
import time
from Token import getERC20Abi
time1 = time.time()
# w3 = Web3(Web3.HTTPProvider("https://ropsten.infura.io/v3/33fa04b1caa64b03b119d504eaf35b53"))
w3 = Web3(Web3.HTTPProvider("http://127.0.0.1:8545/"))
MVIIndex = w3.eth.contract(address="0xd8EF3cACe8b4907117a45B0b125c68560532F94D", abi=getMVIIndexAbi())
walletAddress = Web3.toChecksumAddress('0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266')
recipient = Web3.toChecksumAddress('0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266')

WETHToken = w3.eth.contract(address='0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2', abi=getERC20Abi())
MVIToken = w3.eth.contract(address='0x72e364F2ABdC788b7E918bc238B21f109Cd634D7', abi=getMVIIAbi())
ENJToken = w3.eth.contract(address='0xF629cBd94d3791C9250152BD8dfBDF380E2a3B9c', abi=getERC20Abi())
private_key = '0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80'
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
transaction_1 = MVIToken.functions.approve('0xd8EF3cACe8b4907117a45B0b125c68560532F94D', 284027558590444887222).buildTransaction({
        'chainId':31337,
        'from': walletAddress,
        'gas': 70000,
        'maxFeePerGas': 57562177587,
        'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
        'nonce': 73
    })
signed_txn_1 = w3.eth.account.sign_transaction(transaction_1, private_key=private_key)
# w3.eth.send_raw_transaction(signed_txn_1.rawTransaction)
print("Luong MVI da improve cho MVIIndex:")
print(MVIToken.functions.allowance(Web3.toChecksumAddress('0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266'), '0xd8EF3cACe8b4907117a45B0b125c68560532F94D').call())
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
transaction_2 = MVIIndex.functions.redeem('0x72e364F2ABdC788b7E918bc238B21f109Cd634D7', 284027558590444887222, recipient).buildTransaction({
        'chainId':31337,
        'gas': 3000000,
        'from': walletAddress,
        'maxFeePerGas': 57562177587,
        'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
        'nonce': 74
    })
signed_txn_2 = w3.eth.account.sign_transaction(transaction_2, private_key=private_key)
# w3.eth.send_raw_transaction(signed_txn_2.rawTransaction)
transaction_3 = MVIIndex.functions.issue('0x72e364F2ABdC788b7E918bc238B21f109Cd634D7', 284027558590444887222, recipient).buildTransaction({
        'chainId':31337,
        'gas': 3000000,
        'from': walletAddress,
        'maxFeePerGas': 57562177587,
        'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
        'nonce': 75
    })
signed_txn_3 = w3.eth.account.sign_transaction(transaction_3, private_key=private_key)
# w3.eth.send_raw_transaction(signed_txn_3.rawTransaction)
print(ENJToken.functions.balanceOf(walletAddress).call())