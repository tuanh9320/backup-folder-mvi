from web3 import Web3
from interface import getInterface
import threading
from SetTokenAbi import getSetTokenAbi
import time
from Token import getERC20Abi
time1 = time.time()
# w3 = Web3(Web3.HTTPProvider("https://ropsten.infura.io/v3/33fa04b1caa64b03b119d504eaf35b53"))
w3 = Web3(Web3.HTTPProvider("http://127.0.0.1:8545/"))
uniswap = w3.eth.contract(address="0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D", abi=getInterface())#UniswapV2Router02
sushiswap = w3.eth.contract(address="0xd9e1cE17f2641f24aE83637ab66a2cca9C378B9F", abi=getInterface())#UniswapV2Router02

walletAddress = Web3.toChecksumAddress('0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266')
recipient = Web3.toChecksumAddress('0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266')



WETHToken = w3.eth.contract(address='0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2', abi=getERC20Abi())
private_key = '0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80'
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
def swapToken(token, quantity, typeSwap, nonce):
    token_ = w3.eth.contract(address=token, abi=getERC20Abi())
    uniswap_ = w3.eth.contract(address="0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D", abi=getInterface())#UniswapV2Router02
    sushiswap_ = w3.eth.contract(address="0xd9e1cE17f2641f24aE83637ab66a2cca9C378B9F", abi=getInterface())#UniswapV2Router02
    if typeSwap == 1:
        transaction_1 = token_.functions.approve('0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D', quantity).buildTransaction({
                'chainId':31337,
                'from': walletAddress,
                'gas': 70000,
                'maxFeePerGas': 57562177587,
                'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
                'nonce': nonce
        })
        signed_txn_1 = w3.eth.account.sign_transaction(transaction_1, private_key=private_key)
        w3.eth.send_raw_transaction(signed_txn_1.rawTransaction)
        #>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        timestamp = int((time.time() + 100000000000)//1)
        transaction_2 = uniswap_.functions.swapExactTokensForTokens(quantity,0, 
            [token, "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2"], 
            recipient,timestamp).buildTransaction({
                'chainId':31337,
                'gas': 150000,
                'from': walletAddress,
                'maxFeePerGas': 57562177587,
                'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
                'nonce': nonce + 1
            })
        signed_txn_2 = w3.eth.account.sign_transaction(transaction_2, private_key=private_key)
        w3.eth.send_raw_transaction(signed_txn_2.rawTransaction)
    else:
        transaction_1 = token_.functions.approve('0xd9e1cE17f2641f24aE83637ab66a2cca9C378B9F', quantity).buildTransaction({
                'chainId':31337,
                'from': walletAddress,
                'gas': 150000,
                'maxFeePerGas': 57562177587,
                'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
                'nonce': nonce
        })
        signed_txn_1 = w3.eth.account.sign_transaction(transaction_1, private_key=private_key)
        w3.eth.send_raw_transaction(signed_txn_1.rawTransaction)
        #>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
        timestamp = int((time.time() + 100000000000)//1)
        transaction_2 = sushiswap_.functions.swapExactTokensForTokens(quantity,0, 
            [token, "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2"], 
            recipient,timestamp).buildTransaction({
                'chainId':31337,
                'gas': 3000000,
                'from': walletAddress,
                'maxFeePerGas': 57562177587,
                'maxPriorityFeePerGas': w3.toWei('1', 'gwei'),
                'nonce': nonce + 1
            })
        signed_txn_2 = w3.eth.account.sign_transaction(transaction_2, private_key=private_key)
        w3.eth.send_raw_transaction(signed_txn_2.rawTransaction)
SetToken = w3.eth.contract(address="0x72e364F2ABdC788b7E918bc238B21f109Cd634D7", abi=getSetTokenAbi())#UniswapV2Router02
tokenAddress = SetToken.functions.getComponents().call()
def getAmountInWallet():
    result = []
    for i in tokenAddress:
        contractOfToken = w3.eth.contract(address=i, abi=getERC20Abi())
        result.append(contractOfToken.functions.balanceOf(walletAddress).call())
    return result
amountInWallet = getAmountInWallet()
print(amountInWallet)
# for i in range(14):
#     if i in [1, 6, 7, 8, 11, 12]:
#         swapToken(tokenAddress[i],amountInWallet[i], 2, nonce)
#     else:
#         swapToken(tokenAddress[i],amountInWallet[i], 1, nonce)
#     nonce = nonce + 2
# i = 13
# nonce = 75 + i*2
# typeSwap_ = 0
# if i in [1, 6, 7, 8, 11, 12]:
#     typeSwap_ = 2
# else:
#     typeSwap_ = 1
# print(typeSwap_)
# swapToken(tokenAddress[i],amountInWallet[i], typeSwap_, nonce)
# ENJToken = w3.eth.contract(address=tokenAddress[i], abi=getERC20Abi())
# print(ENJToken.functions.balanceOf(walletAddress).call())
print(WETHToken.functions.balanceOf(walletAddress).call())